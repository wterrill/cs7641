from math import sqrt, acos, pi, pow, degrees
from decimal import Decimal, getcontext
getcontext().prec = 30

class Vector(object):

    CANNOT_NORMALIZE_ZERO_VECTOR_MSG = 'Cannot normalize the zero vector'

    def __init__(self, coordinates):
        try:
            if not coordinates:
                raise ValueError
            self.coordinates = tuple([Decimal(x) for x in coordinates])
            self.dimension = len(coordinates)

        except ValueError:
            raise ValueError('the coordiantes must be nonempty')

        except TypeError:
            raise TypeError('The coordinates must be an iterable')

    def __str__(self):
        return 'Vector: {}'.format(self.coordinates)

    def __eq__(self,v):
        return self.coordinates == v.coordinates


    def add(self,v):
        try:
            if len(self.coordinates)<>len(v.coordinates):
                raise ValueError
            final = []
            for i in range(len(self.coordinates)):
                final.append(self.coordinates[i]+v.coordinates[i])
            return final
        
        except ValueError:
            raise ValueError('The vector size should be the same')


    def sub(self,v):
        try:
            if len(self.coordinates)<>len(v.coordinates):
                raise ValueError
            final = []
            for i in range(len(self.coordinates)):
                final.append(self.coordinates[i]-v.coordinates[i])
            return final
        
        except ValueError:
            raise ValueError('The vector size should be the same')

    def scalar(self,scalar):

        final = []
        for i in range(len(self.coordinates)):
            final.append(self.coordinates[i]*scalar)
        return final
        
    def magnitude(self):
        sum = 0
        for x in self.coordinates:
            sum = sum + pow(x,2)
        return sqrt(sum)

    def normalize(self):
        try:
            mag = self.magnitude()
            return self.scalar(Decimal('1.0')/Decimal(mag))

        except ZeroDivisionError:
            raise Exception(self.CANNOT_NORMALIZE_ZERO_VECTOR_MSG)

    def dot(self,v):
        new_coordinates = [x*y for x,y in zip(self.coordinates,v.coordinates)]
        return sum(new_coordinates)

    def angle_rad(self,v):
        dotprod = self.dot(v)
        magA = self.magnitude()
        magB = v.magnitude()

        return acos(dotprod/Decimal((magA*magB)))

    def angle_deg(self,v):
        dotprod = self.dot(v)
        magA = self.magnitude()
        magB = v.magnitude()

        return degrees(acos(dotprod/Decimal((magA*magB))))

    def is_parallel(self,v,tol):
        if self.magnitude() < 1e-10:
            return True
        elif v.magnitude() < 1e-10:
            return True        
        elif self.angle_deg(v) < 180 + tol and self.angle_deg(v) > 180 - tol:
            return True
        elif self.angle_deg(v) < 0 + tol and self.angle_deg(v) > 0 - tol:
            return True

        else:
            return False

    def is_orth(self,v):
        if abs(self.dot(v)) < 1e-10:
            return True
        else:
            return False

    def get_project(self,v):

    #V parallel = v dot normalization of B  times normalization of b  (multiply by -1 for theta > 90)
        normalized = Vector(v.normalize())
        print "angle"
        print self.angle_deg(v)
        return (normalized.scalar(self.dot(normalized)))

    def get_orth(self,v):
        V = Vector(self.get_project(v))
        orth = self.sub(V)
        return orth  #V - Vparallel = Vorth


a = Vector([7.887, 4.138])
b = Vector([-8.802,6.776])
print a.dot(b)

a = Vector([-5.955,-4.904,-1.874])
b = Vector([-4.496,-8.755,7.103])
print a.dot(b)

a = Vector([3.183,-7.627])
b = Vector([-2.668,5.319])
print a.angle_rad(b)

a = Vector([7.35,0.221,5.188])
b = Vector([2.751,8.259,3.985])
print a.angle_deg(b)

print "---"
a = Vector([-7.579, -7.99])
b = Vector([22.737,23.64])
print a.is_parallel(b,0.5)
print a.is_orth(b)
print "---"
a = Vector([-2.029,9.97,4.172])
b = Vector([-9.231,-6.639,-7.245])
print a.is_parallel(b,0.5)
print a.is_orth(b)
print "---"
a = Vector([-2.328,-7.284,-1.214])
b = Vector([-1.821,1.072,-2.94])
print a.is_parallel(b,0.5)
print a.is_orth(b)
print "---"
a = Vector([2.118,4.827])
b = Vector([0,0])
print a.is_parallel(b,0.5)
print a.is_orth(b)

a = Vector([3.039,1.879])
b = Vector([0.825,2.036])
print a.get_project(b)

a = Vector([-9.88,-3.264,-8.159])
b = Vector([-2.155,-9.353,-9.473])
print a.get_orth(b)

a = Vector([3.009,-6.172,3.692,-2.51])
b = Vector([6.404,-9.144,2.759,8.718])
print "---"
print Vector(a.get_project(b)) 
print "---"
print Vector(a.get_orth(b))
print "---"